FROM bitnami/minideb:stretch
# FROM bitnami/minideb:jessie

LABEL maintainer="@ManuelLR <manuellr.git@gmail.com>"



ENV BUILD_PACKAGES git g++ build-essential qt5-qmake qt5-default qttools5-dev-tools
ENV RUNTIME_PACKAGES libqt5dbus5 libqt5network5 libqt5core5a libqt5widgets5 libqt5gui5 openssl ca-certificates

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /usr/src/

RUN apt update \
    && apt install -y $BUILD_PACKAGES \
    && df -h \
    && rm -rf /var/lib/apt/lists/* \
    && df -h \
    && apt-get clean \
    && df -h

